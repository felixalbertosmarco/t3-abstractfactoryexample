import {ProductB2} from './ProductB2.js';
import {ProductA2} from './ProductA2.js';
import {AbstractFactory} from './AbstractFactory.js';
export class ConcreteFactory2 extends AbstractFactory{
  constructor(){
    super();
    console.log("Soy la factoría concreta 2");
  }
  public createProductA():ProductA2{
   return new ProductA2(); 
  }
  public createProductB():ProductB2{
    return new ProductB2();
  }
}

