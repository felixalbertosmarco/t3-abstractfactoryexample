import {AbstractProductB} from './AbstractProductB.js';
import {AbstractProductA} from './AbstractProductA.js';
import {AbstractFactory} from './AbstractFactory.js';
export class Client{
  private productA:AbstractProductA;
  private productB:AbstractProductB;
  constructor(factory:AbstractFactory){
    this.productA = factory.createProductA();
    this.productB = factory.createProductB();
  }
}
