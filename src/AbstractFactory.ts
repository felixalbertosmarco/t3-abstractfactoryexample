import {AbstractProductB} from './AbstractProductB.js';
import {AbstractProductA} from './AbstractProductA.js';
export abstract class AbstractFactory{
  constructor(){
  }
  public abstract createProductA():AbstractProductA;
  public abstract createProductB():AbstractProductB;
}

