import {ProductB1} from './ProductB1.js';
import {ProductA1} from './ProductA1.js';
import {AbstractFactory} from './AbstractFactory.js';
export class ConcreteFactory1 extends AbstractFactory{
  constructor(){
    super();
    console.log("Soy la factoría concreta 1");
  }
  public createProductA():ProductA1{
   return new ProductA1(); 
  }
  public createProductB():ProductB1{
    return new ProductB1();
  }
}

